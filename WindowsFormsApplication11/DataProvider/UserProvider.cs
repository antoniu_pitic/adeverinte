﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication11.DataProvider
{
    class UserProvider
    {
        static public List<string> nameList = new List<string>();

        static public List<Grupe> grupeList = new List<Grupe>();
        static public List<Person> personList = new List<Person>();

        static public List<string> GetAllFullNames()
        {
            return personList.Select(x => x.Name + " " + x.Surname)
                .ToList();
        }

        static public List<Person> GetPersonsByName(string text)
        {
            return personList
                .Where(x => x.Name.Contains(text) || x.Surname.Contains(text))
                .ToList();

        }
        static public List<Person> GetPersonsByNameAndGrupa(string partOfNAmme, int idgrupa)
        {
            return GetPersonsByGrupa(idgrupa)
                .Where(x => x.Name.Contains(partOfNAmme) || x.Surname.Contains(partOfNAmme))
                .ToList();

        }

        internal static List <Person> GetPersonsByGrupa(int idGrupa)
        {
            if (idGrupa > 0)
            {
                return personList
                    .Where(x => x.Grupa == GetGrupaByIdGrupa(idGrupa))
                    .ToList();
            }
            else{
                return personList;
            }
        }

        static public List<string> GetFullNamesContainsStartsWith(string text)
        {
            return nameList.Where(x => x.StartsWith(text)).ToList();
        }

        internal static void LoadDataBuffer(string dbConnectionString)
        {

            using (OleDbConnection connection = new OleDbConnection(dbConnectionString))
            {
                // Create the Command and Parameter objects.
                PopulateGrupe(connection);
                PopulatePersons(connection);

            }


        }

        private static void PopulateGrupe(OleDbConnection connection)
        {
            OleDbCommand command = new OleDbCommand("SELECT * FROM Grupe", connection);
            connection.Open();
            OleDbDataReader reader = command.ExecuteReader();

            grupeList.Clear();
            while (reader.Read())
            {
                grupeList.Add(
                new Grupe
                {
                    IdGrupa = Convert.ToInt32(reader["idGrupa"]),
                    Grupa = Convert.ToInt32(reader["Grupa"])
                });
            }
            reader.Close();
            connection.Close();
        }



        private static void PopulatePersons(OleDbConnection connection)
        {
                OleDbCommand command = new OleDbCommand(
       "SELECT * FROM Studenti INNER JOIN Grupe ON Studenti.idGrupa = Grupe.idGrupa", connection);


            connection.Open();
            OleDbDataReader reader = command.ExecuteReader();

            personList.Clear();
            while (reader.Read())
            {
                personList.Add(
                new Person
                {
                    IdCode = Convert.ToInt32(reader["studentId"]),
                    Name = Convert.ToString(reader["name"]).ToUpper(),
                    Surname = reader["surname"].ToString().ToUpper(),
                    Grupa = Convert.ToInt32(reader["Grupa"])
                });
            }
            reader.Close();

            connection.Close();
        }

        private static int GetGrupaByIdGrupa(int idGrupa)
        {
            if (idGrupa>0)
            {
                return grupeList.Where(x => x.IdGrupa == idGrupa).FirstOrDefault().Grupa;
            }
            else
            {
                return 0;
            }
        }

        public static string GetSurnameFromFullName(string fullName)
        {
            return Helpers.StringHelper.EraseFirstWord(fullName);
        }



        public static string GetNameFromFullName(string fullName)
        {
            return fullName.Split(' ').First();
        }
    }
}