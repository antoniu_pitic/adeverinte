﻿namespace WindowsFormsApplication11.DataProvider
{
    public class Person
    {
        #region data
        string name;
        string surname;
        int idCode;
        #endregion data

        #region sets'n'gets
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }

            set
            {
                surname = value;
            }
        }

        public int IdCode
        {
            get
            {
                return idCode;
            }

            set
            {
                idCode = value;
            }
        }

        public int Grupa { get; internal set; }

        #endregion
    }
}