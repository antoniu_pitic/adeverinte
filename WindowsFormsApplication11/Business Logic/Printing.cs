﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication11.DataProvider;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApplication11.Business_Logic
{
    static class Printing
    {
        static public void PrintPersons(List<Person> personList)
        {
            var excelApp = new Excel.Application();

            var workbook = excelApp.Workbooks.Open(@"c:\temp\test.xlsx");
            var sheet = workbook.Sheets[1];

            string originalText = sheet.Cells[7, 2].Value2;

            if (personList?.Count > 0)
            {
                originalText = originalText.Replace("<<nume_prenume>>", personList[0].Name + " " + personList[0].Surname);
                sheet.Cells[7, 2].Value2 = originalText;
            }

            bool userDidntCancel =
    excelApp.Dialogs[Excel.XlBuiltInDialog.xlDialogPrint].Show();
            workbook.Close();

        }
    }
}
